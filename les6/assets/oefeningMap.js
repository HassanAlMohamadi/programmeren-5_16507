
const myMap = L.map('map');

const myBasemap = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 19,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets',
    accessToken: 'sk.eyJ1IjoiYmVuZGllcmNreCIsImEiOiJjanA4azJxaDExdWpmM3BwaGp2b2s1MGQ0In0.6SrE7ieBNSKWCME4N_nCYA'
});

myBasemap.addTo(myMap);


myMap.setView([51.219448, 4.402464], 15);

const request = new XMLHttpRequest();
request.open('GET', 'assets/oefeningMap.json', true);

request.onload = function() {
    const data = JSON.parse(this.response);

    const neighborhoodCount = data.winkels.reduce((sums, winkel) => {
        sums[winkel.neighborhood] = (sums[winkel.neighborhood] || 0) + 1;
        return sums;
    }, {});

    const sidebar = document.getElementById('neighborhoods');
    const h3 = document.createElement("h3");
    h3.innerHTML = "Neighborhood In Antwerpen";
    sidebar.appendChild(h3);

    for (let neighborhood in neighborhoodCount) {
        const p = document.createElement("p");

        p.innerHTML = `<b>${neighborhood}</b> : ${neighborhoodCount[neighborhood]}`;
        sidebar.appendChild(p);
    }

    const winkels = data.winkels.map(winkel => {
        L.marker([winkel.lat, winkel.long]).bindPopup(`
        <h2>${winkel.name}</h2>
        <p><b>Neighborhood:</b> ${winkel.neighborhood}</p>
        <p><b>Address:</b> ${winkel.address}</p>
        <p><b>GSM:</b> ${winkel.GSM}</p>
    `).openPopup().addTo(myMap);
    })
}

request.send();