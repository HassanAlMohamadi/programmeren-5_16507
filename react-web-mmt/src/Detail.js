import React, {Component} from 'react';
import Map from "./Map.js";

class Detail extends Component {


    render() {
        let url = `/images/small/${this.props.data.image}`;
        let alt = ` foto van ${this.props.data.name}`;
        
        return (
           
           <div className="container DCon">
                <div className="row">
                    <div className="col-md-4">
                        <h1 className="text-danger">{this.props.data.name}</h1><hr/>
                        <div><span>Address : </span>{this.props.data.straat} | {this.props.data.gemeente} {this.props.data.postcode}</div>
                        <div><span>Email   : </span> {this.props.data.email}</div>
                        <div><span>Link    : </span><a href={`${this.props.data.link}`} target="_blank" rel="noopener noreferrer">Visit Bibliothek </a></div>
                        <div><span>GSM     : </span>{this.props.data.telefoon}</div>
                        <div className="Com-Text">{this.props.data.comment}</div>
                        <div><button className="btn btn-info" onClick={()=> this.props.action('List')}>Bibliotheken List</button></div>
                    </div>
            
                    <div className="col-md-8 myImg">
                        <img className=" myImg" src={url} alt={alt}/>
                    </div>
                </div>
                <hr/>
            
                <div className="row ">
                     <div className="col-md-12">
                       <Map data={this.props.data}/>
                    </div>
                </div>
                
               <div className="row DCon">
                    <div className="col-md-12">
                    
                    </div>
                </div>
           </div>
           
        );
    }
}

export default Detail; 


