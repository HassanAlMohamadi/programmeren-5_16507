var bonus = new Promise(function(resolve, reject) {
   // do a thing, possibly async, then…
   var timer = setInterval(function() {
       let totaal = parseInt(document.querySelector('#totaal-bedrag').innerText);
       if (totaal > 8) {
           resolve(totaal * 2);
           clearInterval(timer);       }
   }, 1000);

});

bonus.then(function(result) {
    document.querySelector('#totaal-bedrag').innerText = result;}
);
