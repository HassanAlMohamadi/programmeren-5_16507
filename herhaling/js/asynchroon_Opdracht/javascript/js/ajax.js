window.onload = function () {
    var ajax = new Ajax();
    ajax.getRequest('https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=5e50ffa08294f9673a4876bb8738ab43',
        showMarvelCharacters);
}

var loadMarvelCharacters = function (that) {
    var ajax;
    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
    }
    else {
        // code for older browsers
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }

    // callback functie
    ajax.onreadystatechange = function () {
        // server sent response and
        // OK 200 The request was fulfilled.
        if (ajax.readyState == 4 && ajax.status == 200) {
            // we maken een tabel element
            showMarvelCharacters(this.responseText);
        }
    };
    ajax.open("GET", "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=5e50ffa08294f9673a4876bb8738ab43", true);
    ajax.send();
}

function showMarvelCharacters(response) {
    // convert JSON to array
    // we zijn alleen geïnteresseerd in de personnages
    var characters = JSON.parse(response).data.results;
    var table = document.createElement('table');
    characters.map(function (character) {
        var row = document.createElement('tr');
        var name = document.createElement('td');
        var textContent = document.createTextNode(character.name);
        var imageCol = document.createElement('td');
        var image = document.createElement('img');
        image.src = character.thumbnail.path + '.' +
            character.thumbnail.extension;
        image.style = "height: 150px;";
        imageCol.appendChild(image);
        name.appendChild(textContent);
        row.appendChild(imageCol);
        row.appendChild(name);
        table.appendChild(row);
    });
    document.body.appendChild(table);
}

window.onload = function () {
    loadMarvelCharacters();
}