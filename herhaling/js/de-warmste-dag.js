var totaal = 0;
var bewerking;
var bewerkingen = '+-*/^';
var getal = '';


let numericKeypadController = function(e) {
    if (e.target.tagName == 'DIV') {
        // alleen als op een div in de numeric-keybad container
        // wordt geklikt.
        if (e.target.parentNode.className == 'numeric-keypad') {
            let content = e.target.innerText;
            // als content in de bewerkingen string zit
            if (bewerkingen.indexOf(content) >= 0) {
                bewerking = content;
            }
            else if (content == '=') {
                // alleen als er eerst een bewerking is gekozen.
                if (bewerking !== undefined) {
                    switch (bewerking) {
                        case '+':
                            optellen(getal);
                            break;
                        case '-':
                            aftrekken(getal);
                            break;
                        case '*':
                            product(getal);
                            break;
                        case '/':
                            deling(getal);
                            break;
                        case '^':
                            macht(getal);
                            break;
                    }
                    document.querySelector('#totaal-bedrag').innerText = totaal;
                    
                }
            }
            else {
                getal = parseInt(content);
                document.querySelector('#display').innerText = getal;
            }
        }
    }
}

let numericKeypad = document.querySelector('.numeric-keypad');
numericKeypad.addEventListener('click', numericKeypadController, false);

let optellen = function(getal) {
    totaal += getal;
};

let aftrekken = function(getal) {
    totaal -= getal;
};

let product = function(getal) {
    totaal *= getal;
};

let deling = function(getal) {
    totaal /= getal;
};

let macht = function(getal) {
    totaal = totaal + Math.pow(totaal, getal);
};
