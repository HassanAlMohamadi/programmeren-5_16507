import React, { Component } from 'react';

class Api extends Component {
    state = {
        data: []
    };


    componentDidMount() {
        const url = "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=1cbf7ce1c0f9d0847a18f6dd2fd16c4b";


        fetch(url)
            .then(result => result.json())
            .then(result => {
                this.setState({
                    data: result.data.results
                })
            }).catch(function (result)
                {alert('Error: ' + result);} );
       }

        render() {
                const { data } = this.state;
                const thumbnail = {height: "100px"};
                const result = data.map((character) => {
                let src = `${character.thumbnail.path}.${character.thumbnail.extension}`;
                let alt = `Afbeelding van ${character.name}`;
                return 
                        <tr key={character.id}>
                            <td>{character.name}</td>
                            <td><img style={thumbnail} src={src} alt={alt}/></td>
                        </tr>;
            });
            return <table>{result}</table>;
    }
}

export default Api;