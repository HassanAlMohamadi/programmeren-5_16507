import React, { Component } from 'react';
import data from './data/neolithicum.json'

class List extends Component {
    list = data.curiosity;

    row = this.list.map((item) => {
        return (
            <tr>
                <td>{item.name}</td>
                <td> <button onClick={() => this.props.action('detail, 1')}>Detail</button></td>
            </tr>
        );
    });
    

    render() {
        return (
            <table>
                {this.row}
            </table>
        );
    }
}

export default List;