import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import List from './List';

class App extends Component {
  route = 'home';
  handleClick = action => {
      alert('handleClick' + action);
      this.route = action;
      this.setState({ action: this.route });
  };

  render() {
    let view;
    //alert(this.route);
    if (this.route === 'home') {
      view = <Home action={this.handleClick} />
    } else {
      view = <List action={this.handleClick} />
    }

    return (
      <div>
        {view}
      </div>
    );
  }
}

export default App;
