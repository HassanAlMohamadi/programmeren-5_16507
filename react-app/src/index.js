import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AppState from './mmt/App';


ReactDOM.render(<AppState/>, document.getElementById('root'));

