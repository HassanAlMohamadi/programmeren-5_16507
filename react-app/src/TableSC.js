import React from 'react';

// simple components
export const Caption = () => {
    return (
        <caption>Jommeke's strips (eigen component)</caption>
    );
}

export const THead = () => {
    return (
        <thead>
            <tr>
                <th scope="col">Nummer</th>
                <th scope="col">Titel</th>
                <th scope="col">Kaft</th>
                <th scope="col">&euro;</th>
            </tr>
        </thead>
        );
}

export const TFoot = () => {
    return (
        <tfoot>
            <tr>
                <th scope="col">Nummer</th>
                <th scope="col">Titel</th>
                <th scope="col">Kaft</th>
                <th scope="col">&euro;</th>
            </tr>
        </tfoot>
        )
}

