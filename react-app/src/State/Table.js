import React, {Component} from 'react';

class Table extends Component {
    render() {
        const data = this.props.data;
        return (
            <div>
            <table>
                <caption>{this.props.caption}</caption>
                {data.map((row, index) => {
                    return (                
                    <tr key={index}>
                        <td>{row.nummer}</td>
                        <td>{row.naam}</td>
                        <td>{row.cover}</td>
                        <td>{row.prijs}</td>
                        <td><button onClick={() => this.props.removeAlbum(index)}>Delete</button></td>
                    </tr>)
                })}

            </table>
            <p><cite className="auteur">{this.props.auteur}</cite></p>
            </div>);
    }
}

export default Table;