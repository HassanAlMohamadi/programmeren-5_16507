import React, { Component } from 'react';
import './index.css';
import TableMetSC from './TableMetSC';
import Table from './State/Table';

class AppComponent extends Component {

  render() {
    return (
      <div classnaam="App">
        <h1>Een React App maken met een component</h1>
        <TableMetSC />
      </div>
    );
  }
}

class AppProps extends Component {
  render() {
    const albums = [{
                'nummer': '6',
                'naam: ': 'Het hemelhuis ',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '7',
                'naam': 'De zwarte Bomma',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '8',
                'naam': 'De ooievaar van Begonia',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '9',
                'naam': 'De Schildpaddenschat',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '10',
                'naam': 'De straalvogel',
                'cover': 'Hardcover',
                'prijs': '$ 8.22'
            },
            {
                'nummer': '11',
                'naam': 'De Zonnemummie',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            }
        ];
    
    return (
      <div classnaam="App">
        <h1>Een React App maken met een component en props</h1>
        <Table caption="Upstairs Downstairs karakters" auteur="JI" data={albums}/>
      </div>
    );
  }
}

class AppState extends Component {
      state = { albums:  [{
                'nummer': '6',
                'naam': 'De ooievaar van Begonia',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '7',
                'naam': 'De zwarte Bomma',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '8',
                'naam': 'De ooievaar van Begonia',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '9',
                'naam': 'De Schildpaddenschat',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            },
            {
                'nummer': '10',
                'naam': 'De straalvogel',
                'cover': 'Hardcover',
                'prijs': '$ 8.22'
            },
            {
                'nummer': '11',
                'naam': 'De Zonnemummie',
                'cover': 'Softcover',
                'prijs': '$ 5.22'
            }
        ]};
        
    removeAlbum = index => {
        const albums = this.state.albums.filter((album, i) => {
            return (i !== index);
        });
        this.setState({albums});
    }
  render() {
    return (
      <div classnaam="App">
        <h1>Een React App maken met een component en props</h1>
        <Table caption="Upstairs Downstairs karakters" auteur="Hassan" removeAlbum={this.removeAlbum}
          data={this.state.albums}/>
      </div>
    );
  }
}

export default AppComponent && AppProps && AppState;
