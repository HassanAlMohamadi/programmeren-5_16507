window.onload = function () {
    var ajax = new Ajax();
    ajax.getRequest('https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=1cbf7ce1c0f9d0847a18f6dd2fd16c4b',
        showMarvelCharacters);
}

var loadMarvelCharacters = function (that) {
    var ajax;
    if (window.XMLHttpRequest) {
        ajax = new XMLHttpRequest();
    }
    else {
      
        ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }

 
    ajax.onreadystatechange = function () {

        if (ajax.readyState == 4 && ajax.status == 200) {
            showMarvelCharacters(this.responseText);
        }
    };
    ajax.open("GET", "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=1cbf7ce1c0f9d0847a18f6dd2fd16c4b", true);
    ajax.send();
}

function showMarvelCharacters(response) {
    // convert JSON to array
    // we zijn alleen geïnteresseerd in de personnages
    var characters = JSON.parse(response).data.results;
    var table = document.createElement('table');
    characters.map(function (character) {
        var row = document.createElement('tr');
        var name = document.createElement('td');
        var textContent = document.createTextNode(character.name);
        var imageCol = document.createElement('td');
        var image = document.createElement('img');
        image.src = character.thumbnail.path + '.' +
            character.thumbnail.extension;
        image.style = "height: 150px;";
        imageCol.appendChild(image);
        name.appendChild(textContent);
        row.appendChild(imageCol);
        row.appendChild(name);
        table.appendChild(row);
    });
    document.body.appendChild(table);
}

window.onload = function () {
    loadMarvelCharacters();
}