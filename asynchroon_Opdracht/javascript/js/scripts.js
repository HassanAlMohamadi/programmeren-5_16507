
var loadMarvelCharacters = function (that) {
  
var url = "https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=1cbf7ce1c0f9d0847a18f6dd2fd16c4b";
    fetch(url).
    then(response => {
        return response.json();
    }).then(data => {
        charactersCall.success(data);
    }).catch(err => {
        charactersCall.error(err);
    });
    
}

var charactersCall = {
    success: function(response)
    
    {
        var characters = response.data.results;
        var table = document.createElement('table');
        table.id = 'prettyTable';
        characters.map(function (character) {
            var row = document.createElement('tr');
            var name = document.createElement('td');
            var textContent = document.createTextNode(character.name);
            var imageCol = document.createElement('td');
            var image = document.createElement('img');
            image.src = character.thumbnail.path + '.' + character.thumbnail.extension;
            image.style = "height: 150px;";
            image.id = 'thumb';
            imageCol.appendChild(image);
            name.appendChild(textContent);
            row.appendChild(name);
            row.appendChild(imageCol);
            table.appendChild(row);
        });
    document.body.appendChild(table);
    },
    error: function(err) {
        var errorHeader = document.createElement('h1');
        var errorMessage = document.createElement('p');
        var headerTextContent = document.createTextNode('Whoops, something went terribly wrong :(');
        var messageTextContent = document.createTextNode(err);
        errorHeader.appendChild(headerTextContent);
        errorMessage.appendChild(messageTextContent);
        document.body.appendChild(errorHeader);
        document.body.appendChild(errorMessage);
    }
}

window.onload = loadMarvelCharacters;