import React from "react"

/*function Content() {
    const date = new Date();
    const hours = date.getHours();
    let datetime;
    let styles ={
        fontSize : 50
    }
    
    
             if(hours < 12)
             {
             datetime ='Good morning'
             styles.color= 'red'
             
             }else if (hours > 12 && hours < 17 )
             {
                 datetime ='good evening'
             }else
             {
                 datetime ='good night'
                 styles.color = "#00FFFF"

             }
       
            return (
              <h1 style={styles}>Hello ,{datetime}</h1>
           )
}*/


function Content (props){
    return(
              <div>
              <div className="card" style={{width:300}}>
              <img className="card-img-top" src={props.imgUrl} alt={props.Name}/>
              <div className="card-body">
                <h4 className="card-title">{props.Name}</h4>
                <p className="card-text">{props.Description}</p>
                <a href="http://www.projectbasedtraining.com/upcoming-free-seminar" className="btn btn-primary">See Profile</a>
              </div>
            </div>
            </div>
        
        
        )
}

export default Content