import React, {Component} from 'react'; 



class App extends Component {
    // initialiseer state vooraleer render de eerste keer wordt uitgevoerd
    //
    //
    state = {
        characters: []
    };    
    
    
    componentDidMount (){
      fetch("https://gateway.marvel.com:443/v1/public/characters?limit=100&apikey=1cbf7ce1c0f9d0847a18f6dd2fd16c4b")
      .then(response => response.json())
      .then (response => {
         this.setState({
             characters : response.data.results
         }) 
      })
        
    }
    
    render(){
        const { characters } = this.state;
        // styling the react way
        const thumbnail = {
               height: "100px"
        };
        const result = characters.map((character) => {
            let src = `${character.thumbnail.path}.${character.thumbnail.extension}`;
            let alt = `Afbeelding van ${character.name}`;
            return <tr key={character.id}>
                    <td>{character.name}</td>
                    <td><img style={thumbnail} src={src} alt={alt}/></td>
                   </tr>;
                   
        });
        return <table>{result}</table>;
    }
}


export default App

