function toonJef() {
  var persoon = [];
  persoon['voornaam'] = 'Jef';
  persoon['familienaam'] = 'Inghelbrecht';
  persoon['straat'] = 'Rue des Blancs Manteaux 38';
  persoon['stad'] = 'Paris';
  persoon['postcode'] = '75001';
  persoon['geslacht'] = 'man';
  persoon['naam'] =  function() {return this.voornaam + ' ' + this.familienaam;}

  document.querySelector('legend').innerText = persoon.naam();
  document.getElementById('voornaam').value = persoon.voornaam;
  document.getElementById('familienaam').value = persoon['familienaam'];
  document.getElementById('straat').value = persoon.straat;
  document.getElementById('postcode').value = persoon['postcode'];
  document.getElementById('stad').value = persoon['stad'];
  // een associatieve array (een object) in JavaScript is geen gewone
  // array maar een echt object, je kan de integer index niet gebruiken
  // in de textbox verschijnt dan ook undefined
  document.getElementById('geslachtIndexGetal').value = persoon[4];
  document.getElementById('geslachtIndexNaam').value = persoon['geslacht'];
  document.getElementById('naam').value = persoon['naam']();
}

function toonKees() {
  var persoon = {
  voornaam: 'Kees',
  familienaam: 'Baten',
  straat: 'Rue des Blancs Manteaux 38',
  stad: 'Paris',
  postcode: '75001',
  geslacht: 'vrouw',
  naam: function() {return this.voornaam + ' ' + this.familienaam;}
  }
  
  document.querySelector('legend').innerText = persoon.naam();
  document.getElementById('voornaam').value = persoon.voornaam;
  document.getElementById('familienaam').value = persoon['familienaam'];
  document.getElementById('straat').value = persoon.straat;
  document.getElementById('postcode').value = persoon['postcode'];
  document.getElementById('stad').value = persoon['stad'];
  // een associatieve array (een object) in JavaScript is geen gewone
  // array maar een echt object, je kan de integer index niet gebruiken
  document.getElementById('geslachtIndexGetal').value = persoon[4];
  document.getElementById('geslachtIndexNaam').value = persoon['geslacht'];
  document.getElementById('naam').value = persoon['naam']();
}

function toonIndexGetal() {
var my_array = ['appel', 'peer', 'sinaasappel', 'ananas',
       'citroen', 'mango'];
  document.getElementById('feedback').innerText = my_array[4];
}

function toonKeesGenest() {
  var persoon = {
    voornaam: 'Kees',
    familienaam: 'Baaten',
    adres : {
      straat: 'Rue des Blancs Manteaux 38',
      stad: 'Paris',
      postcode: '75001'
    },
    geslacht: 'vrouw',
    naam: function() {return this.voornaam + ' ' + this.familienaam;},
    woontIn: fuction() {return this.voornaam + ' woont in ' + this.adres.stad;}
  }
  document.getElementById('feedback').innerText = persoon.woontin();
}