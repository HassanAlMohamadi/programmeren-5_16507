import React, { Component } from 'react';





const TableHeader = () => {
    return (
        <thead>
            <TableHeadings />
        </thead>
    );
}



const TableBody = props => {
    const rows = props.albumData.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.nummer}</td>
                <td>{row.titel}</td>
                <td>{row.kaft}</td>
                <td>{row.prijs}</td>
                <td><button onClick={() => props.removeAlbum(index)}>Delete</button></td>
           </tr>
        );
    });

    return <tbody>{rows}</tbody>;
}



const TableHeadings = () => {
    return (
        <tr>
            <th scope="col">Nummer</th>
            <th scope="col">Titel</th>
            <th scope="col">Kaft</th>
            <th scope="col">&euro;</th>
        </tr>
    );
}



const TableFooter = () => {
    return (
        <tfoot>
            <TableHeadings />
        </tfoot>
    );
}

const TableCaption = () => {
    return (
        <caption>
            Jommeke strips
        </caption>
    );
}


class Table extends Component {
    render() {
        const {albumData, removeAlbum} = this.props;
        
        return (
            <table>
                <TableCaption />
                <TableHeader />
                <TableBody
                    albumData={albumData}
                    removeAlbum = {removeAlbum}
                />
                <TableFooter />
            </table>
        );
    }
}





export default Table;