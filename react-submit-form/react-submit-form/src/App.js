import React, { Component } from 'react';
import Table from './Table';
import Form from './Form';




class App extends Component {
    state = {
        albums: []
    };
    
    
    
    
    
    handleSubmit = album => {
    this.setState({albums: [...this.state.albums, album]});
    }
    render() {
        // const { albums } = this.state;
        return (
           <div className="container">
              <Table
                albumData={this.state.albums}
                removeAlbum={this.removeAlbum}
              />
             <Form handleSubmit={this.handleSubmit} />

          </div>
        )
    }
};
   
   
   

   
   




export default App;