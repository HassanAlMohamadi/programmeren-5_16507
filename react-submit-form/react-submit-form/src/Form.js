import React, { Component } from 'react';



class Form extends Component {
    
    constructor(props) {
        super(props);

        this.initialState = {
            nummer: '',
            titel: '',
            kaft: '',
            prijs: ''
        };
        
        this.state = this.initialState;
    }
    
    handleChange = event => {
    const { name, value } = event.target;

    this.setState({
        [name]: value
    });
    }
    
    submitForm = () => {
    this.props.handleSubmit(this.state);
    this.setState(this.initialState);
    }
    
    
    render() {
    const { nummer, titel, kaft, prijs } = this.state;

    return (
        <form>
            <label>Nummer</label>
            <input
                type="text"
                name="nummer"
                value={nummer}
                onChange={this.handleChange} />
            <label>Titel</label>
            <input
                type="text"
                name="titel"
                value={titel}
                onChange={this.handleChange}/>
            <label>Kaft</label>
            <input
                type="text"
                name="kaft"
                value={kaft}
                onChange={this.handleChange}/>
            <label>Prijs</label>
            <input
                type="text"
                name="prijs"
                value={prijs}
                onChange={this.handleChange}/>
                <input 
                    type="button" 
                    value="Submit" 
                    onClick={this.submitForm} />
        </form>
    );
  }
}

export default Form;